﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyBus = BusTime.Models.BusModels;

namespace BusTime.Controllers
{
    public class BusController : Controller
    {
        // GET: Bus
        public ActionResult NextBus(string start, string end)
        {

            string url = "http://toeibus.azurewebsites.net/api/bus/" + start;
            MyBus mybus = new MyBus();

            //var next_bus = mybus.GetNextBus(url);
            var next_bus = mybus.GetNextBus(start);
            ViewBag.Next2Bus = "後続のバス : " + mybus.GetNext2Bus();
            ViewBag.keisan = mybus.GetNextBus_for_calculation(next_bus);
            ViewBag.Current_time = DateTime.Now.AddHours(9).ToString("H:mm:ss");
            ViewBag.route = start + " -> " + end;
            ViewBag.NextBus = "次のバス : " + next_bus;

            return View("NextBus");
        }
    }
}