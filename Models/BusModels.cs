﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;


namespace BusTime.Models
{

    public class BusTimeTable
    {
        public Dictionary<string, List<int>> koreda { get; set; }
    }

    public class BusModels
    {


        public int NextBus { get; set; }
        public string MyBusStop { get; set; }

        public string NextBus_for_calculation { get; set; }

        public bool Add_33_true { get; set; }


        private List<int> Next2Bus { get; set; }


        public List<string> TimeList { get; set; }

        public BusModels()
        {
            Add_33_true = false;
        }

        public string GetNext2Bus()
        {
            string mono = string.Empty;

            // when shihatu is next bus, currently there is node code that will retrieve next bus from shihatu.
            try
            {
                Next2Bus.RemoveAt(0);
                foreach (var item in Next2Bus)
                {
                    mono += PrettifyBusTime(item.ToString()) + ",";
                }
                mono = mono.Remove(mono.Length - 1);
            }
            catch (Exception)
            {

                mono = "";
            }

            
            return mono;
        }         
        public string GetNextBus_for_calculation(string next_bus)
        {

            int hour_to_add = 9;
            if (Add_33_true)
            {
                // 24(1day) + 9 = 33
                hour_to_add = 33;
            }
            //make return Data like this and you are happy
            var mono = DateTime.Now.AddHours(hour_to_add).ToString("MMMM dd yyyy ") + next_bus + " GMT+9:00";
            return mono;
            //return "April 15 2016 6:31 GMT+9:00";
        }


        private static string GetJson(string start)
        {
            string mono = string.Empty;
            if (start.Equals("harumi"))
            {
                //mono = @"{""foo"":""json"", ""bar"":100, ""nest"":{ ""foobar"":true } }";
                mono = @"{""time"":[631,639,651,657,703,713,718,723,727,731,735,741,745,749,753,757,801,805,809,813,817,822,827,832,837,842,847,852,857,902,908,914,920,926,933,939,946,953,1000,1007,1014,1021,1030,1037,1045,1053,1103,1113,1123,1133,1143,1153,1204,1215,1226,1238,1249,1300,1310,1320,1330,1338,1347,1357,1407,1418,1428,1438,1448,1458,1508,1518,1529,1540,1549,1558,1606,1613,1620,1626,1632,1638,1644,1650,1657,1702,1707,1713,1718,1723,1728,1735,1741,1747,1753,1759,1805,1811,1818,1826,1834,1841,1848,1855,1902,1909,1916,1922,1928,1938,1948,1958,2007,2017,2027,2037,2047,2057,2107,2117,2126,2136,2146,2206]}";
            } else
            {
                mono = @"{""time"":[647,701,714,727,738,751,803,811,818,825,832,839,846,853,901,909,917,928,940,953,1006,1019,1031,1043,1056,1109,1123,1138,1153,1210,1227,1244,1301,1318,1333,1350,1406,1423,1439,1456,1512,1525,1537,1551,1604,1615,1627,1639,1650,1659,1708,1718,1728,1739,1750,1801,1812,1823,1836,1850,1905,1922,1939,1956,2015,2035,2055,2117,2142,2207,2232]}";
            }
            return mono;
        }
        private static string GetTime(string uri)
        {

            //var json = String.Empty;
            //using (WebClient wc = new WebClient())
            //{
            //    json = wc.DownloadString(uri);
            //}

            //return json.ToString();
            return BusModels.GetJson(uri);
        }

        public string GetNextBus(string url)
        {

            string minutes = "0";
            if (DateTime.Now.Minute < 10)
            {
                minutes += "0" + minutes;
            }
            else
            {
                minutes = DateTime.Now.Minute.ToString();
            }

            DateTime japan_time = DateTime.Now.AddHours(9);
            var current_time = Convert.ToInt32((japan_time.Hour) + "" + minutes, 10);
            var next_bus = 0;
            string time_table_json = BusModels.GetTime(url);
            string shihatu = string.Empty;
            int int_shihatu = 0;
            dynamic list = JsonConvert.DeserializeObject(time_table_json);
            int i = 0;

            List<int> _next2bus = new List<int>();

            foreach (var item in list.time)
            {
                if (i == 0)
                {
                    shihatu = item;
                    int_shihatu = item;
                }
                if (current_time < (int)item)
                {
                    _next2bus.Add((int)item);
                    Next2Bus = _next2bus;
                    if (_next2bus.Count == 3)
                    {
                        next_bus = _next2bus[0];
                        break;

                    }
                }
                i++;

            }

            if (next_bus == 0)
            {
                if (int_shihatu < current_time )
                {
                    Add_33_true = true;
                }
                return PrettifyBusTime(shihatu);

            }
            return PrettifyBusTime(next_bus.ToString());

        }


        private string PrettifyBusTime(string bustime)
        {
            if (bustime.Length == 3)
            {
                return String.Format("{0}:{1}", bustime[0], bustime.Substring(1, 2));
            }
            else
            {
                // meaning 4 digits
                return String.Format("{0}:{1}", bustime.Substring(0, 2), bustime.Substring(2, 2));
            }
        }

    }
}