﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BusTime.Startup))]
namespace BusTime
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
